<?php /*
	
@package wakerlytheme

*/

get_header(); ?>

	
	<div class="main_section"><!-- start of main_setion -->
		<div class="col-xs-12">
			<div class="row" style="padding-top: 70px; padding-bottom: 5px">
	  		<header>
	  			<p class="text-center" style="font-family:kreonbold; font-size: 30px; color: rgb(150, 145, 189)">JOIN IN TO</p>
	  		</header>
  			</div>
			<div class="row" style="padding-top: 5px; padding-bottom: 5px">
	  		<header>
	  			<p class="text-center" style="font-family:kreonbold; font-size: 60px; color: white">WAKERLY-CAROLS CHRISTMAS CELEBERATION</p>
	  		</header>
  			</div>
			<div class="row" style="padding-top: 5px; padding-bottom: 5px">
	  		<header>
	  			<p class="text-center" style="font-family:kreonbold; font-size: 30px; color: rgb(150, 145, 189)">THIS DECEMBER 2016</p>
	  		</header>
  			</div>
  			<div class="container fluid" style="padding-top: 40px; padding-bottom: 0px;  position: relative">
  				<header>
	  			<p class="text-center" style="font-family:kreonbold; font-size: 20px; color: rgb(120, 113, 170)">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
	  			</header>
  			</div>
		</div>
		
	</div> <!-- end of main_section -->


	<div class="white_section"> <!-- start of white section -->
		<div class="container fluid" style="padding-top: 20px; padding-bottom: 5px">
	  		<header>
	  			<p class="text-center" style="font-family:kreonbold; font-size: 35px; color: rgb(120, 113, 170)">BE A SPONSOR STATEMENT HERE.IT'LL BE FUN</p>
	  		</header>
  		</div>
  		<div class="row">
  			<button class="wakerly_button" data-toggle="modal" data-target="#myModal">Inquire Us</button>  			
  		</div>
	</div><!-- end of white_section -->

  	<div class="red_section"><!-- start of red_section -->
  		<div class="row" style="padding-top: 20px; padding-bottom: 20px">
	  		<header>
	  			<p class="text-center" style="font-family:kreonbold; font-size: 40px; color: white">EVENTS</p>
	  		</header>
  		</div>
  		<div class="container fluid">
  			<div class="col-xs-12">
  			
	  			<div class="row text-center no-margin">

					<?php 
					$currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args = array('posts_per_page' => 3, 'paged' => $currentPage);
					query_posts($args);
					if( have_posts('standard') ): $i = 0;
						
						while( have_posts('standard') ): the_post(); ?>
							
							<?php 
								if($i==0): $column = 12; 
								elseif($i > 0 && $i <= 2): $column = 6; 
								elseif($i > 2): $column = 4;
								endif;
							?>
							
								<div class="col-xs-<?php echo $column; ?> blog-item" style="padding-left:0px ; padding-right: 10px; padding-bottom: 10px; ">
									<?php if( has_post_thumbnail() ):
										$urlImg = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );
									endif; ?>
									<div style="background-image: url(<?php echo $urlImg; ?>); height: 400px; width: 100%; position: relative; background-size: cover;  background-repeat: no-repeat;">
									
											<?php the_title( sprintf('<h1 class="entry-title" style="position:relative; bottom:0px; color:white"><a href="%s">', esc_url( get_permalink() ) ),'</a></h1>' ); ?>
										
										<small style="color:white"><?php echo the_category(' '); ?></small>
									</div>
								</div>
						
						<?php $i++; endwhile; ?>	
						
					<?php endif;
							wp_reset_query();
					?>
					<button style="font-family:kreonbold; color:white; font-size:20px; position:center"><a style="color:rgba(222, 222, 222, 0)" href="<?php echo esc_url( home_url( '/blog' ) ); ?>" rel="home"><?php bloginfo('name'); ?></a>View More</button>
				</div>
	  		</div>
  		</div>
  		  		
  	</div> <!-- end of red-section -->


  	<div class="yellow_section"> <!-- start of yellow_section -->
  		<div class="col-xs-12">
  			<div class="col-xs-2">
		  		
	  		</div>
  			<div class="col-xs-8" style="padding-top: 20px; padding-bottom: 5px">
		  		<header>
		  			<p class="text-center" style="font-family:kreonbold; font-size: 35px; color: rgb(120, 113, 170)">Reinforcement statement here. Should be brief and not empty</p>
		  		</header>
	  		</div>
	  		<div class="col-xs-2">
		  		
	  		</div>
  		</div>
	  		
  	</div> <!-- end of yellow_section -->


  	<div class="blue_section">
  		<div class="element">
	  		<div class="container fluid" style="padding-top: 20px; padding-bottom: 5px">
	  			<div class="col-xs-12">
	  				<div class="col-xs-6">
	  					<div class="row">
	  						<header >
	  							<p class="text-center" style="color:rgb(248, 214, 98); font-size:35px; font-family:kreonbold; padding-top: 80px; padding-bottom: 20px">
	  								Convincing statement to win Sponsors/Members/Guest.
	  							</p>
	  						</header>
	  					</div>
	  					<div class="row">
	  						<p class="text-center" style="color:white; font-size:15px; font-family:kreonbold; padding-bottom: 40px">
	  							Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
	  						</p>   						 					
	  					</div>
	  					<div class="row" style="padding-bottom: 80px">
	  						<div class="row">
					  			<button class="wakerly_button" data-toggle="modal" data-target="#myModal">Inquire Us</button>
					  		</div>
	  					</div>
	  					
	  				</div>
	  				<div class="col-xs-6">
		  				<div class="santa"></div>		
	  				</div>
	  			</div>	  
	  		</div>
	  	</div>	
  	</div>


  	<div class="dark_section">
  		<div class="container fluid">
  			<header>
  				<p class="text-center" style="font-family:kreonbold; color:white; font-size:35px">Closing augument or statement here. Should be brief.</p>
  			</header>
  		</div>
  	</div>
  	


  	<!-- MODAL -->

  	<div class="modal fade" id="myModal" role="dialog">
    	<div class="modal-dialog">
      	<!-- Modal content-->
	      	<div class="modal-content">
	        	<div class="modal-header">
		          	<button type="button" class="close" data-dismiss="modal">&times;</button>
		          	<h4 class="modal-title">Modal Header</h4>
	        	</div>
	        	<div class="modal-body">
	          		<p>Some text in the modal.</p>
	        	</div>
	        	<div class="modal-footer">
	          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        	</div>
	     	</div>
      
   		</div>
  	</div>

	
<?php get_footer(); ?>