<?php

/*
	
@package wakerlytheme
	
	========================
		ADMIN PAGE
	========================
*/

function wakerly_add_admin_page() {
	
	//Generate Wakerly Admin Page
	add_menu_page( 'Wakerly Theme Options', 'Wakerly', 'manage_options', 'harold_wakerly', 'wakerly_theme_create_page', get_template_directory_uri() . '/img/wakerly-icon.png', 110 );
	
	//Generate Wakerly Admin Sub Pages
	add_submenu_page( 'harold_wakerly', 'Wakerly Sidebar Options', 'Sidebar', 'manage_options', 'harold_wakerly', 'wakerly_theme_create_page' );
	add_submenu_page( 'harold_wakerly', 'Wakerly Theme Options', 'Theme Options', 'manage_options', 'harold_wakerly_theme', 'wakerly_theme_support_page' );
	add_submenu_page( 'harold_wakerly', 'Wakerly Contact Form', 'Contact Form', 'manage_options', 'harold_wakerly_theme_contact', 'wakerly_contact_form_page' );
	add_submenu_page( 'harold_wakerly', 'Wakerly CSS Options', 'Custom CSS', 'manage_options', 'harold_wakerly_css', 'wakerly_theme_settings_page');
	
}
add_action( 'admin_menu', 'wakerly_add_admin_page' );

//Activate custom settings
add_action( 'admin_init', 'wakerly_custom_settings' );

function wakerly_custom_settings() {
	//Sidebar Options
	register_setting( 'wakerly-settings-group', 'profile_picture' );
	register_setting( 'wakerly-settings-group', 'first_name' );
	register_setting( 'wakerly-settings-group', 'last_name' );
	register_setting( 'wakerly-settings-group', 'user_description' );
	register_setting( 'wakerly-settings-group', 'twitter_handler', 'wakerly_sanitize_twitter_handler' );
	register_setting( 'wakerly-settings-group', 'facebook_handler' );
	register_setting( 'wakerly-settings-group', 'gplus_handler' );
	
	add_settings_section( 'wakerly-sidebar-options', 'Sidebar Option', 'wakerly_sidebar_options', 'harold_wakerly');
	
	add_settings_field( 'sidebar-profile-picture', 'Profile Picture', 'wakerly_sidebar_profile', 'harold_wakerly', 'wakerly-sidebar-options');
	add_settings_field( 'sidebar-name', 'Full Name', 'wakerly_sidebar_name', 'harold_wakerly', 'wakerly-sidebar-options');
	add_settings_field( 'sidebar-description', 'Description', 'wakerly_sidebar_description', 'harold_wakerly', 'wakerly-sidebar-options');
	add_settings_field( 'sidebar-twitter', 'Twitter handler', 'wakerly_sidebar_twitter', 'harold_wakerly', 'wakerly-sidebar-options');
	add_settings_field( 'sidebar-facebook', 'Facebook handler', 'wakerly_sidebar_facebook', 'harold_wakerly', 'wakerly-sidebar-options');
	add_settings_field( 'sidebar-gplus', 'Google+ handler', 'wakerly_sidebar_gplus', 'harold_wakerly', 'wakerly-sidebar-options');
	
	//Theme Support Options
	register_setting( 'wakerly-theme-support', 'post_formats' );
	register_setting( 'wakerly-theme-support', 'custom_header' );
	register_setting( 'wakerly-theme-support', 'custom_background' );
	
	add_settings_section( 'wakerly-theme-options', 'Theme Options', 'wakerly_theme_options', 'harold_wakerly_theme' );
	
	add_settings_field( 'post-formats', 'Post Formats', 'wakerly_post_formats', 'harold_wakerly_theme', 'wakerly-theme-options' );
	add_settings_field( 'custom-header', 'Custom Header', 'wakerly_custom_header', 'harold_wakerly_theme', 'wakerly-theme-options' );
	add_settings_field( 'custom-background', 'Custom Background', 'wakerly_custom_background', 'harold_wakerly_theme', 'wakerly-theme-options' );
	
	//Contact Form Options
	register_setting( 'wakerly-contact-options', 'activate_contact' );
	
	add_settings_section( 'wakerly-contact-section', 'Contact Form', 'wakerly_contact_section', 'harold_wakerly_theme_contact');
	
	add_settings_field( 'activate-form', 'Activate Contact Form', 'wakerly_activate_contact', 'harold_wakerly_theme_contact', 'wakerly-contact-section' );
	
	//Custom CSS Options
	register_setting( 'wakerly-custom-css-options', 'wakerly_css', 'wakerly_sanitize_custom_css' );
	
	add_settings_section( 'wakerly-custom-css-section', 'Custom CSS', 'wakerly_custom_css_section_callback', 'harold_wakerly_css' );
	
	add_settings_field( 'custom-css', 'Insert your Custom CSS', 'wakerly_custom_css_callback', 'harold_wakerly_css', 'wakerly-custom-css-section' );
	
}

function wakerly_custom_css_section_callback() {
	echo 'Customize wakerly Theme with your own CSS';
}

function wakerly_custom_css_callback() {
	$css = get_option( 'wakerly_css' );
	$css = ( empty($css) ? '/* Wakerly Theme Custom CSS */' : $css );
	echo '<div id="customCss">'.$css.'</div><textarea id="wakerly_css" name="wakerly_css" style="display:none;visibility:hidden;">'.$css.'</textarea>';
}

function wakerly_theme_options() {
	echo 'Activate and Deactivate specific Theme Support Options';
}

function wakerly_contact_section() {
	echo 'Activate and Deactivate the Built-in Contact Form';
}

function wakerly_activate_contact() {
	$options = get_option( 'activate_contact' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="activate_contact" name="activate_contact" value="1" '.$checked.' /></label>';
}

function wakerly_post_formats() {
	$options = get_option( 'post_formats' );
	$formats = array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' );
	$output = '';
	foreach ( $formats as $format ){
		$checked = ( @$options[$format] == 1 ? 'checked' : '' );
		$output .= '<label><input type="checkbox" id="'.$format.'" name="post_formats['.$format.']" value="1" '.$checked.' /> '.$format.'</label><br>';
	}
	echo $output;
}

function wakerly_custom_header() {
	$options = get_option( 'custom_header' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="custom_header" name="custom_header" value="1" '.$checked.' /> Activate the Custom Header</label>';
}

function wakerly_custom_background() {
	$options = get_option( 'custom_background' );
	$checked = ( @$options == 1 ? 'checked' : '' );
	echo '<label><input type="checkbox" id="custom_background" name="custom_background" value="1" '.$checked.' /> Activate the Custom Background</label>';
}

// Sidebar Options Functions
function wakerly_sidebar_options() {
	echo 'Customize your Sidebar Information';
}

function wakerly_sidebar_profile() {
	$picture = esc_attr( get_option( 'profile_picture' ) );
	if( empty($picture) ){
		echo '<button type="button" class="button button-secondary" value="Upload Profile Picture" id="upload-button"><span class="wakerly-icon-button dashicons-before dashicons-format-image"></span> Upload Profile Picture</button><input type="hidden" id="profile-picture" name="profile_picture" value="" />';
	} else {
		echo '<button type="button" class="button button-secondary" value="Replace Profile Picture" id="upload-button"><span class="wakerly-icon-button dashicons-before dashicons-format-image"></span> Replace Profile Picture</button><input type="hidden" id="profile-picture" name="profile_picture" value="'.$picture.'" /> <button type="button" class="button button-secondary" value="Remove" id="remove-picture"><span class="wakerly-icon-button dashicons-before dashicons-no"></span> Remove</button>';
	}
	
}
function wakerly_sidebar_name() {
	$firstName = esc_attr( get_option( 'first_name' ) );
	$lastName = esc_attr( get_option( 'last_name' ) );
	echo '<input type="text" name="first_name" value="'.$firstName.'" placeholder="First Name" /> <input type="text" name="last_name" value="'.$lastName.'" placeholder="Last Name" />';
}
function wakerly_sidebar_description() {
	$description = esc_attr( get_option( 'user_description' ) );
	echo '<input type="text" name="user_description" value="'.$description.'" placeholder="Description" /><p class="description">Write something smart.</p>';
}
function wakerly_sidebar_twitter() {
	$twitter = esc_attr( get_option( 'twitter_handler' ) );
	echo '<input type="text" name="twitter_handler" value="'.$twitter.'" placeholder="Twitter handler" /><p class="description">Input your Twitter username without the @ character.</p>';
}
function wakerly_sidebar_facebook() {
	$facebook = esc_attr( get_option( 'facebook_handler' ) );
	echo '<input type="text" name="facebook_handler" value="'.$facebook.'" placeholder="Facebook handler" />';
}
function wakerly_sidebar_gplus() {
	$gplus = esc_attr( get_option( 'gplus_handler' ) );
	echo '<input type="text" name="gplus_handler" value="'.$gplus.'" placeholder="Google+ handler" />';
}

//Sanitization settings
function wakerly_sanitize_twitter_handler( $input ){
	$output = sanitize_text_field( $input );
	$output = str_replace('@', '', $output);
	return $output;
}

function wakerly_sanitize_custom_css( $input ){
	$output = esc_textarea( $input );
	return $output;
}

//Template submenu functions
function wakerly_theme_create_page() {
	require_once( get_template_directory() . '/inc/templates/wakerly-admin.php' );
}

function wakerly_theme_support_page() {
	require_once( get_template_directory() . '/inc/templates/wakerly-theme-support.php' );
}

function wakerly_contact_form_page() {
	require_once( get_template_directory() . '/inc/templates/wakerly-contact-form.php' );
}

function wakerly_theme_settings_page() {
	require_once( get_template_directory() . '/inc/templates/wakerly-custom-css.php' );
}


















