<h1>Wakerly Custom CSS</h1>
<?php settings_errors(); ?>

<form id="save-custom-css-form" method="post" action="options.php" class="wakerly-general-form">
	<?php settings_fields( 'wakerly-custom-css-options' ); ?>
	<?php do_settings_sections( 'harold_wakerly_css' ); ?>
	<?php submit_button(); ?>
</form>