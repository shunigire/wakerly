<h1>Wakerly Contact Form</h1>
<?php settings_errors(); ?>

<form method="post" action="options.php" class="wakerly-general-form">
	<?php settings_fields( 'wakerly-contact-options' ); ?>
	<?php do_settings_sections( 'harold_wakerly_theme_contact' ); ?>
	<?php submit_button(); ?>
</form>