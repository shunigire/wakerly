<h1>Wakerly Theme Support</h1>
<?php settings_errors(); ?>

<form method="post" action="options.php" class="wakerly-general-form">
	<?php settings_fields( 'wakerly-theme-support' ); ?>
	<?php do_settings_sections( 'harold_wakerly_theme' ); ?>
	<?php submit_button(); ?>
</form>