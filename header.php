<?php 
	
	/*
		This is the template for the header
		
		@package wakerlytheme
	*/
	
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<title><?php bloginfo( 'name' ); wp_title(); ?></title>
		<meta name="description" content="<?php bloginfo( 'description' ); ?>">
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if( is_singular() && pings_open( get_queried_object() ) ): ?>
			<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		<?php wp_head(); ?>
		
		<?php 
			$custom_css = esc_attr( get_option( 'wakerly_css' ) );
			if( !empty( $custom_css ) ):
				echo '<style>' . $custom_css . '</style>';
			endif;
		?>
		
	</head>

<body <?php body_class(); ?>>
	
	<div class="container-fluid">
		
		<div class="row">
				
			<header class="new_header-container" >
				<div class="col-xs-12">
					<div style="height: 70px"></div>					
					<div class="container fluid">
						<!-- <div class="col-xs-3">
							<?php //if ( get_theme_mod( 'custom_logo' ) ) : ?>
								<div>
									<a href="<?php //echo esc_url( home_url( '/' ) ); ?>" title="<?php //echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php //echo esc_url( get_theme_mod( 'custom_logo' ) ); ?>" alt="<?php //echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
								</div>
							<?php //else : ?>
								<hgroup class="identity">
									<h1 class="site-title"><a href="<?php// echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php //bloginfo( 'name' ); ?></a></h1>
									<h2 class="site-description"><?php //bloginfo( 'description' ); ?></h2>
								</hgroup>
							<?php// endif; ?>
						</div>	-->
						<div class="col-xs-2">
							<button class="logo">
								<a style="color:rgba(222, 222, 222, 0)" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo('name'); ?></a>
							</button>							
						</div>
						<div class="col-xs-5">							
							<div class="row" style="position: relative">
								<p style="font-family:kreonbold; font-size: 20px; color: rgb(150, 145, 189); text-align:left; padding-top:15px">20-22 December 2016</p>
							</div>							
							<div class="row" style="position: relative">
								<p style="font-family:kreonbold; font-size: 20px; color: rgb(150, 145, 189); text-align:left">Somewhere, Overther, City</p>
							</div>							
						</div>											
						<div class="col-xs-5">
							<div class="row" style="position: relative">
								<p style="font-family:kreonbold; font-size: 20px; color: rgb(150, 145, 189); text-align:right; padding-top:15px">20-22 December 2016</p>
							</div>							
							<div class="row" style="position: relative">
								<p style="font-family:kreonbold; font-size: 20px; color: rgb(150, 145, 189); text-align:right">Somewhere, Overther, City</p>
							</div>							
						</div>					
					</div>
					
					<div class="nav-container">
						
						<nav class="navbar new_navbar-wakerly navbar-fixed-top">
							<?php
								wp_nav_menu( array(
									'theme_location' => 'primary',
									'container' => false,
									'menu_class' => 'nav navbar-nav',
									'walker' => new Wakerly_Walker_Nav_Primary()
								) );	
							?>
						</nav>
						
					</div><!-- .nav-container -->
				</div>									
			</header><!-- .header-container -->

		</div><!-- .row -->
		
	</div><!-- .container-fluid -->
	
	
	

	