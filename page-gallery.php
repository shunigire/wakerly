<?php /*
	
@package wakerlytheme

*/

get_header(); ?>
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<?php if( is_paged() ): ?>
			
				<div class="container text-center container-load-previous">
					<a class="btn-wakerly-load wakerly-load-more" data-prev="1" data-page="<?php echo wakerly_check_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
						<span class="wakerly-icon wakerly-loading"></span>
						<span class="text">Load Previous</span>
					</a>
				</div><!-- .container -->
				
			<?php endif; ?>

			<div class="yellow_section">
				<div class="container wakerly-posts-container">
				
					<?php 
						
						if( have_posts() ):
							
							echo '<div class="page-limit" data-page="/' . wakerly_check_paged() . '">';
							
							while( have_posts() ): the_post();
								
								/*
								$class = 'reveal';
								set_query_var( 'post-class', $class );
								*/
								get_template_part( 'template-parts/content', get_post_format() );
							
							endwhile;
							
							echo '</div>';
							
						endif;
	                
					?>
					
				</div><!-- .container -->
			
			</div>
			
			<div class="red_section">
				<div class="container text-center">
					<a class="btn-wakerly-load wakerly-load-more" data-page="<?php echo wakerly_check_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
						<span class="wakerly-icon wakerly-loading"></span>
						<span class="text">Load More</span>
					</a>
				</div><!-- .container -->
			</div>
			
			
			
		</main>
	</div><!-- #primary -->
	
<?php get_footer(); ?>